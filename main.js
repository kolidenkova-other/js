import Counter from './modules/counter.js';
import CounterNew from './modules/counter-new.js'
import {CounterPrototype} from './modules/counter-prototype.js'
import CounterClass from './modules/counter-class.js'

let counters = document.getElementsByClassName('counter');

// Вариант 1
// let counter = new Counter(counters[0]);
// let counter2 = new Counter(counters[1]);

// counter.increase(1);
// counter.reduce(1);
// counter.reset();
// counter2.increase(2);
// counter2.reset();


// Вариант 2
// let a = new CounterNew({
//     'element': counters[0],
//     'increment': 1,
//     'decrement': 1,
//     'reset': true
// });
//
// new CounterNew({
//     'element': counters[1],
//     'increment': 2,
//     'decrement': false,
//     'reset': true
// });

// // Вариант 3 прототипы
// new CounterPrototype({
//     'element': counters[0],
//     'increment': 1,
//     'decrement': 1,
//     'reset': true
// });
//
// new CounterPrototype({
//     'element': counters[1],
//     'increment': 2,
//     'decrement': false,
//     'reset': true
// });

// Вариант 4 классы
new CounterClass({
    'element': counters[0],
    'increment': 1,
    'decrement': 1,
    'reset': true
});

new CounterClass({
    'element': counters[1],
    'increment': 2,
    'decrement': false,
    'reset': true
});
