export default function Counter(element) {
    this.element = element;

    let valueElement = this.element.querySelector('.count'),
        oldValue = parseInt(valueElement.innerHTML);

    let setValue = function (newValue) {
        valueElement.innerHTML = newValue;
        oldValue = newValue;
    };

    this.increase = function (valueIncrement) {
        let inclementElement = this.element.querySelector('.increment');

        if (null !== inclementElement) {
            inclementElement.addEventListener('click', function () {
                let newValue = oldValue + valueIncrement;
                setValue(newValue);
            })
        }

    };

    this.reduce = function (valueDecrement) {
        let decrementElement = this.element.querySelector('.decrement');

        if (null !== decrementElement) {
            decrementElement.addEventListener('click', function () {
                let newValue = oldValue - valueDecrement;
                newValue = 0 > newValue ? 0 : newValue;
                setValue(newValue);
            })
        }
    };

    this.reset = function () {
        let resetElement = this.element.querySelector('.reset');

        if (null !== resetElement) {
            resetElement.addEventListener('click', function () {
                setValue(0);
            })
        }
    }
}