export default class CounterClass {

    constructor(parameters) {
        this.element = parameters.element;
        this.valueIncrement = parameters.increment;
        this.valueDecrement = parameters.decrement;
        this.valueReset = parameters.reset;
        this._valueElement = this.element.querySelector('.count');
        this.value = parseInt(this._valueElement.innerHTML);

        this.initListeners();
    }

    updateView() {
        this._valueElement.innerHTML = this.value;
    };

    increase () {
       this.value = this.value + this.valueIncrement;
       this.updateView();
    };

    reduce() {
        this.value = this.value - this.valueDecrement;
        this.value = 0 > this.value ? 0 : this.value;
        this.updateView();
    };

    reset() {
        this.value = 0;
        this.updateView();
    };

    initListeners() {
        if (false !== this.valueIncrement) {
            let inclementElement = this.element.querySelector('.increment');
            inclementElement.addEventListener('click', this.increase.bind(this))
        }

        if (false !== this.valueDecrement) {
            let decrementElement = this.element.querySelector('.decrement');
            decrementElement.addEventListener('click', this.reduce.bind(this))
        }

        if (false !== this.valueReset) {
            let resetElement = this.element.querySelector('.reset');
            resetElement.addEventListener('click', this.reset.bind(this))
        }
    }
}