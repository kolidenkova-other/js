export default function CounterNew(parameters) {
    this.element = parameters.element;
    this.valueIncrement = parameters.increment;
    this.valueDecrement = parameters.decrement;
    this.valueReset = parameters.reset;

    let valueElement = this.element.querySelector('.count');

    this.oldValue = parseInt(valueElement.innerHTML);

    this.setValue = function (newValue) {
        valueElement.innerHTML = newValue;
        this.oldValue = newValue;
    };

    this.increase = function () {
        let newValue = this.oldValue + this.valueIncrement;
        this.setValue(newValue);
    };

    this.reduce = function () {
        let newValue = this.oldValue - this.valueDecrement;
        newValue = 0 > newValue ? 0 : newValue;
        this.setValue(newValue);
    };

    this.reset = function () {
        this.setValue(0);
    };

    if (false !== this.valueIncrement) {
        let inclementElement = this.element.querySelector('.increment');
        inclementElement.addEventListener('click', this.increase.bind(this))
    }

    if (false !== this.valueDecrement) {
        let decrementElement = this.element.querySelector('.decrement');
        decrementElement.addEventListener('click', this.reduce.bind(this))
    }

    if (false !== this.valueReset) {
        let resetElement = this.element.querySelector('.reset');
        resetElement.addEventListener('click', this.reset.bind(this))
    }
}