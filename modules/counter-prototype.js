export let CounterPrototype = function (parameters) {
    this.element = parameters.element;
    this.valueIncrement = parameters.increment;
    this.valueDecrement = parameters.decrement;
    this.valueReset = parameters.reset;
    this._valueElement = this.element.querySelector('.count');
    this.value = parseInt(this._valueElement.innerHTML);

    this.initListeners();
};

CounterPrototype.prototype.updateView = function () {
    this._valueElement.innerHTML = this.value;
};

CounterPrototype.prototype.increase = function () {
    this.value = this.value + this.valueIncrement;
    this.updateView();
};

CounterPrototype.prototype.reduce = function () {
    this.value = this.value - this.valueDecrement;
    this.value = 0 > this.value ? 0 : this.value;
    this.updateView();
};

CounterPrototype.prototype.reset = function () {
    this.value = 0;
    this.updateView();
};

CounterPrototype.prototype.initListeners = function () {
    if (false !== this.valueIncrement) {
        let inclementElement = this.element.querySelector('.increment');
        inclementElement.addEventListener('click', this.increase.bind(this))
    }

    if (false !== this.valueDecrement) {
        let decrementElement = this.element.querySelector('.decrement');
        decrementElement.addEventListener('click', this.reduce.bind(this))
    }

    if (false !== this.valueReset) {
        let resetElement = this.element.querySelector('.reset');
        resetElement.addEventListener('click', this.reset.bind(this))
    }
};